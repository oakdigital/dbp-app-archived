const forms = require("@tailwindcss/forms");
const colors = require('tailwindcss/colors');

const config = {
  mode: "jit",
  content: ["./src/**/*.{html,js,svelte,ts}"],

  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#0E2542",
          dark: "#31455B",
          darker: "#0D1D32",
        },
        secondary: {
          DEFAULT: "#D69330",
          light: "#dea95b",
        },
        faded: {
          DEFAULT: "#E8EEF5",
          blue: "#828C98",
        },
        danger: {
          DEFAULT: colors.red[500],
        },
      },
      fontFamily: {
        assistant: ["Assistant", "sans-serif"],
        roboto: ["Roboto", "sans-serif"],
      },
      borderRadius: {
        DEFAULT: "5px",
      },
      fontSize: {
        header: ["25px"],
      },
      padding: {
        main: ["25px"],
        header: ["20px"],
        large: ["20px"],
        medium: ["10px"],
        text: ["5px"],
      },
      gridTemplateColumns: {
        guide: "30% 70%",
      },
    },
  },

  plugins: [forms],
};

module.exports = config;
