//import adapter from "@sveltejs/adapter-auto";
// import adapter from "@sveltejs/adapter-node";
import adapter from "@sveltejs/adapter-static";
import preprocess from "svelte-preprocess";
import path from "path";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [
    preprocess({
      postcss: true,
    }),
  ],

  kit: {
    adapter: adapter(),
    //target: '#svelte',

    // Override http methods in the Todo forms
    methodOverride: {
      allowed: ["PATCH", "DELETE"],
    },
    vite: {
      resolve: {
        alias: {
          $comp: path.resolve("./src/components"),
        },
      },
      mode: process.env.MODE || process.env.NODE_ENV || "production",
    },
  },
};

export default config;
