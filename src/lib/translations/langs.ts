import dk from "./da/common.json";
import en from "./en/common.json";
import se from "./se/common.json";

export default {
  dk: {
    common: dk,
  },
  en: {
    common: en,
  },
  se: {
    common: se,
  },
};
