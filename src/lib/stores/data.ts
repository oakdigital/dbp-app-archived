import { getSalesforceData } from "$lib/Utility";
import { writable, readable, derived, get } from "svelte/store";
import Booking from "$lib/data-classes/Booking";

export let data = writable({
  totalSize: 1,
  done: true,
  records: [],
});

function getBookings(data: any): Array<Booking> {
  const bookings = data.records.map((record: any) => {
    return new Booking(record);
  });
  return bookings;
}

export const fetchAndUpdateData = async (bookings) => {
  const booking = bookings[0];
  const response = await getSalesforceData(
    booking.Account.PersonEmail,
    booking.BookingNumber
  );

  if (response.status_code != "200") {
    throw new Error("could not get correct data from slaesforce");
  }

  data.set(response.data);
  sessionStorage.setItem("booking_data", JSON.stringify(response.data));

  return response.data;
};

export const bookings = derived(data, ($data) => {
  return getBookings($data);
});

const init = async () => {
  if (typeof sessionStorage === "undefined") {
    return;
  }
  let dataString: any;
  try {
    dataString = JSON.parse(sessionStorage.getItem("booking_data"));
  } catch (ex) {
    sessionStorage.setItem("booking_data", "");
    return;
  }
  // console.log(dataString);
  if (dataString == null || dataString == undefined) {
    return;
  }
  data.update(() => {
    return dataString;
  });
};

export let initPromise = readable(init());
