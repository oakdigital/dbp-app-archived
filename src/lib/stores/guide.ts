import { bookings } from "$lib/stores/data";
import { get, writable, derived, readable } from "svelte/store";
import Guide from "$lib/data-classes/Guide";

export let tGuide = writable({
  data: {},
  isFetched: false,
});

export const tripGuide = derived(tGuide, ($tGuide) => {
  return new Guide($tGuide.data);
});

export let guideLangChange = async function language(locale: string) {
  let localeString = "";
  if (locale != "dk") {
    localeString = `${locale}/`;
  }
  let endpoint = `${
    import.meta.env.VITE_BOOKING_API_URL
  }${localeString}wp-json/dbp-engine/v1/trip/read/first`;
  return await getGuide(endpoint);
};

export let fetchTripData = async function fetchTripData(endpoint) {
  let trip = {
    trip_id: get(bookings)[0].Trip_id,
  };
  const queryParams = new URLSearchParams(trip);
  let fetchedData;
  const response = await fetch(`${endpoint}?${queryParams}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => (fetchedData = response.json()))
    .catch((error) => {
      throw new Error(error);
    });
  return fetchedData;
};

export let getGuide = async function GetGuide(endpoint) {
  let fetchedData = await fetchTripData(endpoint);
  if (fetchedData?.data && fetchedData.data?.status !== 200) {
    throw new Error("No guide found");
  }

  return tGuide.update((d) => {
    d.isFetched = true;
    d.data = fetchedData;
    return d;
  });
};

export const keys = readable([
  "Practical",
  "Payments",
  "Program",
  "ImportantInformation",
  "AfterJourney",
  "Media",
  "Questions",
]);

export const isScrolling = writable(false);
