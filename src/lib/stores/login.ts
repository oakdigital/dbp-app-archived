import { writable, get } from "svelte/store";

export const email = writable("");
export const booking_number = writable("");

const init = () => {
  if (typeof localStorage === "undefined") return;

  try {
    const loadedEmail = localStorage.getItem("login_email");
    email.update(() => loadedEmail);
  } catch (ex) {
    localStorage.setItem("login_email", "");
  }
  const envEmail = import.meta.env.VITE_DEFAULT_LOGIN_EMAIL;
  if (envEmail && !get(email)) {
    email.update(() => envEmail);
    localStorage.setItem("login_email", envEmail);
  }

  try {
    const loaded = localStorage.getItem("login_booking_number");
    booking_number.update(() => loaded);
  } catch (ex) {
    localStorage.setItem("login_booking_number", "");
  }
  const envBookingNumber = import.meta.env.VITE_DEFAULT_LOGIN_BOOKING_NUMBER;
  if (envBookingNumber && !get(booking_number)) {
    booking_number.update(() => envBookingNumber);
    localStorage.setItem("login_booking_number", envBookingNumber);
  }
};

init();
