export default class  {
    LocaleNameObject: any = {};

    constructor(namesObject, endsWithString: string) {
        const nameKeys = Object.keys(namesObject)
            .filter(key => key.endsWith(endsWithString));

        nameKeys.forEach(key => {
            let localeKey = key.substring(0, 2).toLowerCase();
            this.LocaleNameObject[localeKey] = namesObject[key];
        });

    }

    getLocalName(locale: string) {
        const name = this.LocaleNameObject[locale];
        if (typeof name !== "string") {
            // TODO: fallback language
        }

        return name;
    }
}
