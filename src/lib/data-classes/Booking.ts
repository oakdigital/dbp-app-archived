import Account from "./Account";
import Addon from "./Addon";
import Trip from "./Trip";
import Payment, { PaymentCategory } from "./Payment";
import AgeGroup from "./AgeGroup";

export default class {
  Account: Account;
  BasePrice: number;
  Payments: Array<Payment>;
  DisplayPayments: Array<Payment>; // The payments that are to be displayed in a list
  Addons: Array<Addon>;
  AgeGroups: Array<AgeGroup>;
  FullPayment: Payment | null = null;
  Currency: string;
  Departure: Date;
  Arrival: Date;
  Price: number;
  Id: string;
  BookingNumber: string;
  Trip: Trip;
  Trip_id: string;
  Days: number;
  CurrencyMap: string;

  constructor(bookingObject: any) {
    this.BasePrice = bookingObject.Base_Price__c;
    this.Currency = bookingObject.CurrencyIsoCode;
    this.Departure = this.changeTimeZone(
      bookingObject.Departure__c,
      "Europe/Copenhagen"
    );
    this.Arrival = this.changeTimeZone(
      bookingObject.Arrival__c,
      "Europe/Copenhagen"
    );
    this.Price = bookingObject.Price__c;
    this.Account = new Account(bookingObject.Account__r, this);
    this.Id = bookingObject.Id;
    this.Trip_id = bookingObject.Trip__c;
    this.BookingNumber = bookingObject.Name;
    this.Days = bookingObject.Number_Of_Nights__c;

    const paymentRecords = bookingObject.Payments__r?.records ?? [];
    this.Payments = paymentRecords.map((record: any) => {
      return new Payment(record, this);
    });

    this.CurrencyMap = this.currencyMapper();

    // this.DisplayPayments = this.Payments.filter(
    //   (payment) => payment.Category !== PaymentCategory.FullPayment
    // );
    this.DisplayPayments = this.Payments;

    this.FullPayment = this.Payments.find(
      (payment) => payment.Category === PaymentCategory.FullPayment
    );

    this.sortDisplayPayments();

    const addonRecords = bookingObject.Booking_Addons__r?.records ?? [];
    this.Addons = addonRecords.map((record: any) => new Addon(record, this));

    this.Trip = new Trip(bookingObject.Trip__r, this);

    this.AgeGroups =
      bookingObject.Booking_Age_Groups__r?.records.map(
        (record: any) => new AgeGroup(record, this)
      ) ?? [];
  }

  changeTimeZone(date, timeZone) {
    if (typeof date === "string") {
      return new Date(
        new Date(date).toLocaleString("en-US", {
          timeZone,
        })
      );
    }

    return new Date(
      date.toLocaleString("en-US", {
        timeZone,
      })
    );
  }

  currencyMapper() {
    switch (this.Currency) {
      case "DKK":
        return "";
      case "SEK":
        return "se/";
      case "EUR":
        return "en/";
    }
  }

  sortDisplayPayments() {
    this.DisplayPayments.sort((payA, payB) => payA.Category - payB.Category);
  }

  hasRemainingPayments() {
    return this.Payments.some((payment) => !payment.Paid);
  }

  hasNoPayments() {
    return this.Payments.length === 0;
  }

  hasOnlyFullPayment() {
    return (
      this.Payments.length === 1 &&
      this.Payments.every(
        (payment) => payment.Category === PaymentCategory.FullPayment
      )
    );
  }

  getPaidAmount(): number {
    // if full payment is paid, return the price of that
    if (this.FullPayment?.Paid) {
      return this.FullPayment.Amount;
    }

    // sum the paid payments
    return this.Payments.reduce((acc, payment) => {
      return acc + payment.Amount * Number(payment.Paid);
    }, 0);
  }

  getRemainingPrice() {
    return this.DisplayPayments.reduce((acc, payment) => {
      return acc + payment.Amount * Number(!payment.Paid);
    }, 0);
  }

  // This includes already paid payments
  getTotalPrice() {
    return this.Payments.reduce((acc, payment) => {
      return acc + payment.Amount;
    }, 0);
  }
}
