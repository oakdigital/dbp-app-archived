import type Booking from "./Booking";
import { formatCurrency } from "$lib/Utility";
import Payment from "$lib/header/SVG/payment.svelte";

export const enum PaymentStatus {
  New,
  Linked,
  Approved,
  Captured,
  Invoiced,
  Failed,
  CaptureFailed
}

export const enum PaymentCategory {
  DepositPayment = 0,
  RestPayment = 1,
  AddonPayment = 2,
  FullPayment = 3, // should always be last
}

// str could be null from salesforce
function stringStatusToEnum(str: string | null): PaymentStatus {
  switch (str) {
    case "Linked":
      return PaymentStatus.Linked;
    case "Approved":
      return PaymentStatus.Approved;
    case "Captured":
      return PaymentStatus.Captured;
    case "Invoiced":
      return PaymentStatus.Invoiced;
    case "Failed":
      return PaymentStatus.Failed;
    case "Capture Failed":
      return PaymentStatus.CaptureFailed;
    case "New":
    default:
      return PaymentStatus.New;
  }
}

// str could be null from salesforce
function stringCategoryToEnum(str: string | null): PaymentCategory {
  switch (str) {
    case "Deposit Payment":
      return PaymentCategory.DepositPayment;
    case "Rest Payment":
      return PaymentCategory.RestPayment;
    case "Full Payment":
      return PaymentCategory.FullPayment;
    case "Addon Payment":
    default:
      return PaymentCategory.AddonPayment;
  }
}

export default class {
  Booking: Booking;
  Category: PaymentCategory;
  Description: string;
  Id: string;
  Name: string;
  Amount: number = 0;
  Status: PaymentStatus;
  DeadlineString: string;
  PaymentEligible: boolean;
  PaymentLink: string | null = null;

  constructor(paymentObject: any, booking: Booking) {
    this.Booking = booking;
    this.Category = stringCategoryToEnum(paymentObject.Category__c);
    this.Description = paymentObject.Description__c;
    this.Id = paymentObject.Id;
    this.Name = paymentObject.Name;
    this.Amount = paymentObject.Amount__c;
    this.DeadlineString = paymentObject.Payment_Deadline__c;
    this.PaymentLink = paymentObject.Payment_Link__c;
    this.PaymentEligible = paymentObject.Payment_Eligibility__c ?? null;

    this.Status = stringStatusToEnum(paymentObject.Status__c);

    // console.log(paymentObject);
  }

  getFullPayment() {
    return this.Booking.FullPayment;
  }

  isFullPayment() {
    return this === this.getFullPayment();
  }

   // Removed 23/06/2022
  // get Payable(): boolean { // Deleted because field is no longer in use. See Payment.PaymentEligible
  //   return this.PaymentEligible; 
  // }

  get Failed(): boolean {
    return this.Status === PaymentStatus.CaptureFailed;
  }

  get Retryable(): boolean {
    return this.Status === PaymentStatus.Failed;
  }

  get Approved(): boolean {
    return this.Status === PaymentStatus.Approved;
  }

  get Paid(): boolean {
    // const fullPayment = this.getFullPayment();  // Removed 24/06/2022

    if (this.Approved) {
      // If it's approved, it's paid
      return true;
    }

    // Removed 24/06/2022 because full payment is now a separate payment.
    // Be careful for infinite recursion
    // If the full payment is paid, then all payments are paid.
    // if (!this.isFullPayment() && fullPayment && fullPayment.Paid) {
    //   return true;
    // }

    if( this.Status === PaymentStatus.Captured) return true;
    if (this.Status === PaymentStatus.Invoiced) return true;
    return false;
  }

  get Currency(): string {
    return this.Booking.Currency;
  }

  daysUntilDeadline() {
    if(this.DeadlineString === null || this.DeadlineString == '') return '';
    const today = new Date();
    const deadline = new Date(this.DeadlineString);
    deadline.setHours(23);
    deadline.setMinutes(59);
    deadline.setSeconds(59);
    const diff = deadline - today;
    const day = 1000 * 60 * 60 * 24;
    const daysUntil = diff / day;
    return daysUntil;
  }

  isDeadlineClose() {
    return this.daysUntilDeadline() < 7;
  }

  formatDeadline(locale: string) {
    if(this.DeadlineString === null || this.DeadlineString == '') return '';
    const date = new Date(this.DeadlineString);
    let options = {
      year: "numeric",
      month: "short",
      day: "numeric",
    };
    let lang: string = "";
    switch (locale) {
      case "en":
        lang = "en-GB";
        break;
      case "se":
        lang = "sv-SE";
        break;
      default:
        lang = "da-DK";
        break;
    }

    return date.toLocaleDateString(lang, options);
  }

  getDescription() {
    switch (this.Category) {
      case PaymentCategory.DepositPayment:
        return "common.payment.deposit";
      case PaymentCategory.RestPayment:
        return "common.payment.rest_payment";
      case PaymentCategory.FullPayment:
        return "common.payment.full_payment";
      case PaymentCategory.AddonPayment:
      default:
        return this.Description;
    }
  }

  formatCurrency() {
    return formatCurrency(this.Amount, this.Currency);
  }
}
