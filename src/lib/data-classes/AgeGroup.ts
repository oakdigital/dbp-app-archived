import BaseLocaleName from "./BaseLocaleName";
import type Booking from "./Booking";
export default class AgeGroup extends BaseLocaleName {
    Booking: Booking;
    Id: string;
    Name: string;
    Amount: number;

    constructor(ageGroupObject, booking: Booking) {
        super(ageGroupObject.Age_Group__r, "_Age_Group_Name__c");
        this.Booking = booking;
        this.Id = ageGroupObject.Id;
        this.Name = ageGroupObject.Name;
        this.Amount = ageGroupObject.Number_Of_Participants__c;
    }
}
