export interface PostObject {
  post_title: string;
  link: string;
  image: string;
}

export interface PracticalList {
  section_title: string;
  section_content: string;
  image: string;
}

export default class {
  Practical: Array<PracticalList>;
  AfterJourney: Array<PracticalList>;
  ImportantInformation: Array<PracticalList>;
  LinkedTrip: PostObject;
  Payments: Array<PracticalList>;
  Media: Array<PracticalList>;
  Program: Array<PracticalList>;
  Questions: Array<PracticalList>;
  Image: string;
  Link: string;

  constructor(guideObject: any) {
    this.Practical = guideObject.practical_info ?? [];
    this.AfterJourney = guideObject.after_your_journey ?? [];
    this.ImportantInformation = guideObject.important_information ?? [];
    this.LinkedTrip = guideObject.link_to_trip ?? {};
    this.Payments = guideObject.payments ?? [];
    this.Media = guideObject.photos_and_videos ?? [];
    this.Program = guideObject.program_for_journey ?? [];
    this.Questions = guideObject.questions ?? [];
  }
}
