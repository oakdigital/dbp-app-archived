import type Booking from "./Booking";

export default class {
    Booking: Booking;
    FirstName: string;
    LastName: string;
    PersonEmail: string;

    constructor(accountObject: any, booking: Booking) {
        this.Booking = booking;
        this.FirstName = accountObject.FirstName;
        this.LastName = accountObject.LastName;
        this.PersonEmail = accountObject.PersonEmail;
    }
}
