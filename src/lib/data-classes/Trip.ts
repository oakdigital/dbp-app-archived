import type Booking from "./Booking";
export default class {
  Booking: Booking;
  Destination: string;

  constructor(tripObject, booking: Booking) {
    this.Booking = booking;
    this.Destination = tripObject.Destination__c;
  }
}
