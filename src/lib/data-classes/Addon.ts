import type Booking from "./Booking";
import BaseLocaleName from "./BaseLocaleName";
import { formatCurrency } from "$lib/Utility";

export default class extends BaseLocaleName {
  Booking: Booking;
  Name: string;
  Amount: number;
  Price: number = 0;
  PricePerDay: boolean = false;

  constructor(addonObject: any, booking: Booking) {
    super(addonObject.Addon__r, "_Addon_Name__c");
    this.Booking = booking;
    this.Price = addonObject.Addon_Converted_Price__c;
    this.Name = addonObject.Name;
    this.Amount = addonObject.Number_Of_Addons__c;
    this.PricePerDay = addonObject.Addon__r.Price_per_day__c;
  }

  get TotalAddonPrice() : number {
    if (this.PricePerDay) {
      return this.Amount * this.Price * this.Booking.Days;
    }
    return this.Price * this.Amount;
  }

  get Currency() {
    return this.Booking.Currency;
  }

  formatCurrency() {
    return formatCurrency(this.TotalAddonPrice, this.Currency);
  }
}
