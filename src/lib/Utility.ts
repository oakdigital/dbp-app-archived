import { locale, t } from "$lib/translations";
import { get } from "svelte/store";
export let stringTrans = function stringTrans(key) {
  let lang = get(locale).toUpperCase();
  return `${lang}_${key}`; // EN_Addon_Name__c
};

function currencyLocaleSwitch(): string {
  switch (get(locale).toUpperCase()) {
    case "DK":
      return "da-DK";
    case "SE":
      return "sv-SE";
    case "EN":
    default:
      return "en-GB";
  }
}

export let formatCurrency = function formatCurrency(
  amount: number | null,
  currency: string
): string {
  if (amount === null || currency === undefined) {
    return get(t)("common.price_missing");
  }
  let currencyLocale = currencyLocaleSwitch();
  return (
    amount.toLocaleString(currencyLocale, {
      style: "currency",
      currency: currency,
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }) || "0"
  );
};

export const getSalesforceData = async (
  email: string,
  booking_number: string
) => {
  const loginData = {
    email,
    booking_number,
  };

  const endpoint = `${
    import.meta.env.VITE_BOOKING_API_URL
  }/wp-json/dbp-engine/v1/booking/get`;
  const response = await fetch(
    `${endpoint}?${new URLSearchParams(loginData)}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const data = await response.json();
  return data;
};
