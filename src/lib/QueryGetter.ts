export default class QueryGetter {
  static readonly _base_queries = {
    mobile: 0,
    sm: 640,
    md: 768,
    lg: 1024,
    xl: 1280,
    "2xl": 1536,
  };

  static readonly queries = {
    ...this._base_queries,
    mobile_max: this._base_queries.sm - 1,
    sm_max: this._base_queries.md - 1,
    md_max: this._base_queries.lg - 1,
    lg_max: this._base_queries.xl - 1,
    xl_max: this._base_queries["2xl"] - 1,
  };

  static getQueryValue(queryKey: string) {
    return this.queries[queryKey];
  }

  static getMaxWidth(queryKey: string): string {
    return `(max-width: ${this.getQueryValue(queryKey)}px)`;
  }

  static getMinWidth(queryKey: string): string {
    return `(min-width: ${this.getQueryValue(queryKey)}px)`;
  }

  static getBetween(fromKey: string, toKey: string): string {
    return `${this.getMinWidth(fromKey)} and ${this.getMaxWidth(toKey)}`;
  }
  
}
